20190323-MohammedMayouf-NYCSchools

Further Improvements 

Add pagination so that only a portion of the data will be collected at a time

Add more test cases for the presenters

Allow the user to re-fetch the data (refresh) since this will only be done once, when the localDB is empty
This can be done by either using a pull-to-refresh or adding a refresh icon to the toolbar. 

