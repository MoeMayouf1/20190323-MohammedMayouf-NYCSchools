package com.challenge.nycschools.ui.nyc_school_detail;

import com.challenge.nycschools.data.pojo.NYCSchool;
import com.challenge.nycschools.data.pojo.NYSchoolDetail;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class SchoolDetailsPresenterTest {

    @Mock
    SchoolDetailsView schoolDetailsView;

    private SchoolDetailsPresenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new SchoolDetailsPresenter();
        presenter.onAttach(schoolDetailsView);
    }

    @Test
    public void showDetails() {
        presenter.getSchoolDetails(getNySchool());

        verify(schoolDetailsView).showSchoolAddress(anyString());
        verify(schoolDetailsView).showPhoneNumber(anyString());
        verify(schoolDetailsView).showSchoolAddress(anyString());
        verify(schoolDetailsView).showSchoolWebsite(anyString());
    }

    public NYSchoolDetail getNySchool() {
        NYSchoolDetail nySchoolDetail = new NYSchoolDetail();
        NYCSchool nycSchool = new NYCSchool();
        nycSchool.setCity("City");
        nycSchool.setPhoneNumber("123456");
        nycSchool.setSchoolEmail("test@email.com");
        nycSchool.setStateCode("CC");
        nycSchool.setZip("12323");
        nycSchool.setWebsite("www.helloWorld.com");
        nySchoolDetail.setSchool(nycSchool);
        nySchoolDetail.setSatScores(anyList());

        return nySchoolDetail;
    }

}