package com.challenge.nycschools.ui.nyc_schools;

import com.challenge.nycschools.R;
import com.challenge.nycschools.common.SchedulerProvider;
import com.challenge.nycschools.data.pojo.NYCSchool;
import com.challenge.nycschools.data.service.base.DataManager;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NYSchoolsPresenterTest {

    private static Scheduler scheduler;

    @Mock
    NYSchoolsView nySchoolsView;
    @Mock
    DataManager dataManager;
    @Mock
    private List<NYCSchool> nycSchoolList;

    private NYSchoolsPresenter presenter;

    @Before
    public void setUp() {
        presenter = new NYSchoolsPresenter(new SchedulerProvider(), dataManager);
        presenter.onAttach(nySchoolsView);
    }

    @BeforeClass
    public static void setUpRxSchedulers() {
        scheduler = new Scheduler() {
            @Override
            public Disposable scheduleDirect(@NonNull Runnable run, long delay, @NonNull TimeUnit unit) {
                // this prevents StackOverflowErrors when scheduling with a delay
                return super.scheduleDirect(run, 0, unit);
            }

            @Override
            public Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(Runnable::run, false);
            }
        };

        RxJavaPlugins.setInitIoSchedulerHandler(sch -> scheduler);
        RxJavaPlugins.setInitComputationSchedulerHandler(sch -> scheduler);
        RxJavaPlugins.setInitNewThreadSchedulerHandler(sch -> scheduler);
        RxJavaPlugins.setInitSingleSchedulerHandler(sch -> scheduler);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(sch -> scheduler);
    }

    @Test
    public void testDataLoadedSuccessfully() {
        when(dataManager.getNYSchools()).thenReturn(Single.just(nycSchoolList));
        presenter.getNYSchools();
        verify(nySchoolsView).showLoading();
        verify(nySchoolsView).setSchoolsList(nycSchoolList);
        verify(nySchoolsView).hideLoading();
    }

    @Test
    public void testFailedLoadingData() {
        when(dataManager.getNYSchools()).thenReturn(Single.error(new Throwable("Something Went Wrong!")));
        presenter.getNYSchools();
        verify(nySchoolsView).hideLoading();
        verify(nySchoolsView).showMessage(R.string.error_message);

    }

}