package com.challenge.nycschools.di.components;

import android.app.Application;

import com.challenge.nycschools.NYCSchoolApp;
import com.challenge.nycschools.di.modules.ActivityBuilderModule;
import com.challenge.nycschools.di.modules.ApplicationModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, ActivityBuilderModule.class,
        ApplicationModule.class})
public interface ApplicationComponent extends AndroidInjector<DaggerApplication> {

    void inject(NYCSchoolApp application);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        ApplicationComponent build();
    }
}
