package com.challenge.nycschools.di.modules;

import com.challenge.nycschools.ui.nyc_school_detail.SchoolDetailsFragment;
import com.challenge.nycschools.ui.nyc_school_detail.SchoolDetailsFragmentModule;
import com.challenge.nycschools.ui.nyc_schools.NYSchoolsFragment;
import com.challenge.nycschools.ui.nyc_schools.NYSchoolsFragmentModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuilderModule {

    @ContributesAndroidInjector(modules = NYSchoolsFragmentModule.class)
    abstract NYSchoolsFragment provideNYSchoolsFragment();

    @ContributesAndroidInjector(modules = SchoolDetailsFragmentModule.class)
    abstract SchoolDetailsFragment provideSchoolDetailFragment();

}
