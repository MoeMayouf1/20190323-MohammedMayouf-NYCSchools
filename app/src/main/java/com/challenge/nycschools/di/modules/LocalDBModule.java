package com.challenge.nycschools.di.modules;

import android.app.Application;

import com.challenge.nycschools.common.AppConstants;
import com.challenge.nycschools.data.service.local.NYSchoolDatabase;
import com.challenge.nycschools.data.service.local.NYSchoolsDao;

import javax.inject.Singleton;

import androidx.room.Room;
import dagger.Module;
import dagger.Provides;

@Module
public class LocalDBModule {

    @Provides
    @Singleton
    NYSchoolDatabase provideRoomDatabase(Application application) {
        return Room.databaseBuilder(application.getApplicationContext(),
                NYSchoolDatabase.class, AppConstants.DB_NAME)
                .build();
    }

    @Provides
    @Singleton
    NYSchoolsDao provideNYSchoolDao(NYSchoolDatabase database) {
        return database.nySchoolsDao();
    }
}
