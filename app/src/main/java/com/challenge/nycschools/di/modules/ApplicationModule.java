package com.challenge.nycschools.di.modules;

import android.content.Context;

import com.challenge.nycschools.NYCSchoolApp;
import com.challenge.nycschools.common.BaseSchedulerProvider;
import com.challenge.nycschools.common.SchedulerProvider;
import com.challenge.nycschools.data.service.DataManagerImpl;
import com.challenge.nycschools.data.service.base.DataManager;
import com.challenge.nycschools.data.service.base.LocalDataSource;
import com.challenge.nycschools.data.service.base.RemoteDataSource;
import com.challenge.nycschools.data.service.local.LocalDataSourceImpl;
import com.challenge.nycschools.data.service.remote.RemoteDataSourceImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module(includes = {NetworkModule.class, LocalDBModule.class})
public class ApplicationModule {

    @Provides
    @Singleton
    Context provideContext(NYCSchoolApp nycSchoolApp) {
        return nycSchoolApp;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(DataManagerImpl  dataManager) {
        return dataManager;
    }

    @Provides
    @Singleton
    RemoteDataSource provideRemoteDataSource(RemoteDataSourceImpl remoteDataSource) {
        return remoteDataSource;
    }

    @Provides
    @Singleton
    LocalDataSource provideLocalDataSource(LocalDataSourceImpl localDataSource) {
        return localDataSource;
    }

    @Provides
    @Singleton
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @Singleton
    BaseSchedulerProvider provideSchedulerProvider() {
        return new SchedulerProvider();
    }

}
