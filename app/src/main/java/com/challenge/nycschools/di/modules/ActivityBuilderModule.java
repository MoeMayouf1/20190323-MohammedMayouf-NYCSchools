package com.challenge.nycschools.di.modules;

import com.challenge.nycschools.ui.nyc_schools.NYSchoolsActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(modules = FragmentBuilderModule.class)
    abstract NYSchoolsActivity bindNYSchoolsActivity();
}
