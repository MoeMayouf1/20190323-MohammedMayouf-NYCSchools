package com.challenge.nycschools.data.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class NYSchoolDetail implements Parcelable {

    private NYCSchool school;

    private List<SchoolSatScore> satScores;

    public NYSchoolDetail() {
    }

    public NYSchoolDetail(NYCSchool school, List<SchoolSatScore> satScores) {
        this.school = school;
        this.satScores = satScores;
    }

    public NYCSchool getSchool() {
        return school;
    }

    public void setSchool(NYCSchool school) {
        this.school = school;
    }

    public List<SchoolSatScore> getSatScores() {
        return satScores;
    }

    public void setSatScores(List<SchoolSatScore> satScores) {
        this.satScores = satScores;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.school, flags);
        dest.writeList(this.satScores);
    }

    protected NYSchoolDetail(Parcel in) {
        this.school = in.readParcelable(NYCSchool.class.getClassLoader());
        this.satScores = new ArrayList<>();
        in.readList(this.satScores, SchoolSatScore.class.getClassLoader());
    }

    public static final Parcelable.Creator<NYSchoolDetail> CREATOR = new Parcelable.Creator<NYSchoolDetail>() {
        @Override
        public NYSchoolDetail createFromParcel(Parcel source) {
            return new NYSchoolDetail(source);
        }

        @Override
        public NYSchoolDetail[] newArray(int size) {
            return new NYSchoolDetail[size];
        }
    };
}
