package com.challenge.nycschools.data.service.local;

import com.challenge.nycschools.data.pojo.NYCSchool;
import com.challenge.nycschools.data.service.base.LocalDataSource;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Maybe;

public class LocalDataSourceImpl implements LocalDataSource {

    private final NYSchoolsDao nySchoolsDao;

    @Inject
    public LocalDataSourceImpl(NYSchoolsDao nySchoolsDao) {
        this.nySchoolsDao = nySchoolsDao;
    }

    @Override
    public Maybe<List<NYCSchool>> getAllSchools() {
        return nySchoolsDao.getAllSchools();
    }

    @Override
    public void addAllSchools(List<NYCSchool> nycSchools) {
        nySchoolsDao.insertSchools(nycSchools);
    }
}
