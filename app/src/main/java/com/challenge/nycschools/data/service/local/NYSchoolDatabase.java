package com.challenge.nycschools.data.service.local;

import com.challenge.nycschools.BuildConfig;
import com.challenge.nycschools.data.pojo.NYCSchool;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = NYCSchool.class, version = BuildConfig.DB_VERSION)
public abstract class NYSchoolDatabase extends RoomDatabase {

    public abstract NYSchoolsDao nySchoolsDao();

}
