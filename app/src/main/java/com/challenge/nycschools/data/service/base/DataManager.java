package com.challenge.nycschools.data.service.base;

import com.challenge.nycschools.data.pojo.NYCSchool;
import com.challenge.nycschools.data.pojo.SchoolSatScore;

import java.util.List;

import io.reactivex.Single;

public interface DataManager {

    Single<List<NYCSchool>> getNYSchools();

    Single<List<NYCSchool>> getSchoolsFromRemote();

    Single<List<SchoolSatScore>> getSatForSchool(String dbn);
}
