package com.challenge.nycschools.data.service;

import com.challenge.nycschools.data.pojo.NYCSchool;
import com.challenge.nycschools.data.pojo.SchoolSatScore;
import com.challenge.nycschools.data.service.base.DataManager;
import com.challenge.nycschools.data.service.base.LocalDataSource;
import com.challenge.nycschools.data.service.base.RemoteDataSource;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class DataManagerImpl implements DataManager {

    private final RemoteDataSource remoteDataSource;
    private final LocalDataSource localDataSource;

    @Inject
    public DataManagerImpl(RemoteDataSource remoteDataSource, LocalDataSource localDataSource) {
        this.remoteDataSource = remoteDataSource;
        this.localDataSource = localDataSource;
    }

    @Override
    public Single<List<NYCSchool>> getNYSchools() {
        return localDataSource.getAllSchools()
                .filter(nycSchools -> !nycSchools.isEmpty())
                .switchIfEmpty(getSchoolsFromRemote());
    }

    @Override
    public Single<List<NYCSchool>> getSchoolsFromRemote() {
        return remoteDataSource.getNYSchools()
                .doOnSuccess(localDataSource::addAllSchools);

    }

    @Override
    public Single<List<SchoolSatScore>> getSatForSchool(String dbn) {
        return remoteDataSource.getSatForSchool(dbn);
    }
}
