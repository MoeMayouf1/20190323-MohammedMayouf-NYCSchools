package com.challenge.nycschools.data.service.remote;

import com.challenge.nycschools.BuildConfig;
import com.challenge.nycschools.data.pojo.NYCSchool;
import com.challenge.nycschools.data.pojo.SchoolSatScore;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NYCSchoolService {

    String NYC_SCHOOLS_ENDPOINT = BuildConfig.RELATIVE_URL_SCHOOLS;
    String NYC_SCHOOL_SAT_ENDPOINT = BuildConfig.RELATIVE_URL_STATS;

    @GET(NYC_SCHOOLS_ENDPOINT)
    Single<List<NYCSchool>> getNYSchools();

    @GET(NYC_SCHOOL_SAT_ENDPOINT)
    Single<List<SchoolSatScore>> getSatForSchool(@Query("dbn") String dbn);
}
