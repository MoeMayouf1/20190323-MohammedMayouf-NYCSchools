package com.challenge.nycschools.data.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SchoolSatScore implements Parcelable {

    @SerializedName("dbn")
    @Expose
    private String dbn;
    @SerializedName("num_of_sat_test_takers")
    @Expose
    private String numOfSatTestTakers;
    @SerializedName("sat_critical_reading_avg_score")
    @Expose
    private String satCriticalReadingAvgScore;
    @SerializedName("sat_math_avg_score")
    @Expose
    private String satMathAvgScore;
    @SerializedName("sat_writing_avg_score")
    @Expose
    private String satWritingAvgScore;
    @SerializedName("school_name")
    @Expose
    private String schoolName;

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public SchoolSatScore withDbn(String dbn) {
        this.dbn = dbn;
        return this;
    }

    public String getNumOfSatTestTakers() {
        return numOfSatTestTakers;
    }

    public void setNumOfSatTestTakers(String numOfSatTestTakers) {
        this.numOfSatTestTakers = numOfSatTestTakers;
    }

    public SchoolSatScore withNumOfSatTestTakers(String numOfSatTestTakers) {
        this.numOfSatTestTakers = numOfSatTestTakers;
        return this;
    }

    public String getSatCriticalReadingAvgScore() {
        return satCriticalReadingAvgScore;
    }

    public void setSatCriticalReadingAvgScore(String satCriticalReadingAvgScore) {
        this.satCriticalReadingAvgScore = satCriticalReadingAvgScore;
    }

    public SchoolSatScore withSatCriticalReadingAvgScore(String satCriticalReadingAvgScore) {
        this.satCriticalReadingAvgScore = satCriticalReadingAvgScore;
        return this;
    }

    public String getSatMathAvgScore() {
        return satMathAvgScore;
    }

    public void setSatMathAvgScore(String satMathAvgScore) {
        this.satMathAvgScore = satMathAvgScore;
    }

    public SchoolSatScore withSatMathAvgScore(String satMathAvgScore) {
        this.satMathAvgScore = satMathAvgScore;
        return this;
    }

    public String getSatWritingAvgScore() {
        return satWritingAvgScore;
    }

    public void setSatWritingAvgScore(String satWritingAvgScore) {
        this.satWritingAvgScore = satWritingAvgScore;
    }

    public SchoolSatScore withSatWritingAvgScore(String satWritingAvgScore) {
        this.satWritingAvgScore = satWritingAvgScore;
        return this;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public SchoolSatScore withSchoolName(String schoolName) {
        this.schoolName = schoolName;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.dbn);
        dest.writeString(this.numOfSatTestTakers);
        dest.writeString(this.satCriticalReadingAvgScore);
        dest.writeString(this.satMathAvgScore);
        dest.writeString(this.satWritingAvgScore);
        dest.writeString(this.schoolName);
    }

    public SchoolSatScore() {
    }

    protected SchoolSatScore(Parcel in) {
        this.dbn = in.readString();
        this.numOfSatTestTakers = in.readString();
        this.satCriticalReadingAvgScore = in.readString();
        this.satMathAvgScore = in.readString();
        this.satWritingAvgScore = in.readString();
        this.schoolName = in.readString();
    }

    public static final Parcelable.Creator<SchoolSatScore> CREATOR = new Parcelable.Creator<SchoolSatScore>() {
        @Override
        public SchoolSatScore createFromParcel(Parcel source) {
            return new SchoolSatScore(source);
        }

        @Override
        public SchoolSatScore[] newArray(int size) {
            return new SchoolSatScore[size];
        }
    };
}