package com.challenge.nycschools.data.service.local;

import com.challenge.nycschools.data.pojo.NYCSchool;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Maybe;

@Dao
public interface NYSchoolsDao {

    @Query("SELECT * FROM nycschool")
    Maybe<List<NYCSchool>> getAllSchools();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSchools(List<NYCSchool> nycSchools);
}
