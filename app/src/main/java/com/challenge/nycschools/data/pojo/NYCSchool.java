package com.challenge.nycschools.data.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class NYCSchool implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("academicopportunities1")
    @Expose
    private String academicopportunities1;
    @SerializedName("academicopportunities2")
    @Expose
    private String academicopportunities2;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("dbn")
    @Expose
    private String dbn;
    @SerializedName("overview_paragraph")
    @Expose
    private String overviewParagraph;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("primary_address_line_1")
    @Expose
    private String primaryAddressLine1;
    @SerializedName("school_email")
    @Expose
    private String schoolEmail;
    @SerializedName("school_name")
    @Expose
    private String schoolName;
    @SerializedName("state_code")
    @Expose
    private String stateCode;
    @SerializedName("school_sports")
    @Expose
    private String totalStudents;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("zip")
    @Expose
    private String zip;

    public NYCSchool() {
    }

    public String getAcademicopportunities1() {
        return academicopportunities1;
    }

    public void setAcademicopportunities1(String academicopportunities1) {
        this.academicopportunities1 = academicopportunities1;
    }

    public String getAcademicopportunities2() {
        return academicopportunities2;
    }

    public void setAcademicopportunities2(String academicopportunities2) {
        this.academicopportunities2 = academicopportunities2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getOverviewParagraph() {
        return overviewParagraph;
    }

    public void setOverviewParagraph(String overviewParagraph) {
        this.overviewParagraph = overviewParagraph;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPrimaryAddressLine1() {
        return primaryAddressLine1;
    }

    public void setPrimaryAddressLine1(String primaryAddressLine1) {
        this.primaryAddressLine1 = primaryAddressLine1;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public void setSchoolEmail(String schoolEmail) {
        this.schoolEmail = schoolEmail;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getTotalStudents() {
        return totalStudents;
    }

    public void setTotalStudents(String totalStudents) {
        this.totalStudents = totalStudents;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.academicopportunities1);
        dest.writeString(this.academicopportunities2);
        dest.writeString(this.city);
        dest.writeString(this.dbn);
        dest.writeString(this.overviewParagraph);
        dest.writeString(this.phoneNumber);
        dest.writeString(this.primaryAddressLine1);
        dest.writeString(this.schoolEmail);
        dest.writeString(this.schoolName);
        dest.writeString(this.stateCode);
        dest.writeString(this.totalStudents);
        dest.writeString(this.website);
        dest.writeString(this.zip);
    }

    protected NYCSchool(Parcel in) {
        this.academicopportunities1 = in.readString();
        this.academicopportunities2 = in.readString();
        this.city = in.readString();
        this.dbn = in.readString();
        this.overviewParagraph = in.readString();
        this.phoneNumber = in.readString();
        this.primaryAddressLine1 = in.readString();
        this.schoolEmail = in.readString();
        this.schoolName = in.readString();
        this.stateCode = in.readString();
        this.totalStudents = in.readString();
        this.website = in.readString();
        this.zip = in.readString();
    }

    public static final Parcelable.Creator<NYCSchool> CREATOR = new Parcelable.Creator<NYCSchool>() {
        @Override
        public NYCSchool createFromParcel(Parcel source) {
            return new NYCSchool(source);
        }

        @Override
        public NYCSchool[] newArray(int size) {
            return new NYCSchool[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NYCSchool)) return false;

        NYCSchool school = (NYCSchool) o;

        return getId() == school.getId();
    }

    @Override
    public int hashCode() {
        return getId();
    }
}