package com.challenge.nycschools.data.service.base;

import com.challenge.nycschools.data.pojo.NYCSchool;

import java.util.List;

import io.reactivex.Maybe;

public interface LocalDataSource {

    Maybe<List<NYCSchool>> getAllSchools();

    void addAllSchools(List<NYCSchool> nycSchools);
}
