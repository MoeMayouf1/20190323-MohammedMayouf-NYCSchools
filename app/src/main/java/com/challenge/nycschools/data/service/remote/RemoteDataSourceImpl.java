package com.challenge.nycschools.data.service.remote;

import com.challenge.nycschools.data.pojo.NYCSchool;
import com.challenge.nycschools.data.pojo.SchoolSatScore;
import com.challenge.nycschools.data.service.base.RemoteDataSource;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class RemoteDataSourceImpl implements RemoteDataSource {

    private final NYCSchoolService nycSchoolService;

    @Inject
    public RemoteDataSourceImpl(NYCSchoolService nycSchoolService) {
        this.nycSchoolService = nycSchoolService;
    }

    @Override
    public Single<List<NYCSchool>> getNYSchools() {
        return nycSchoolService.getNYSchools();
    }

    @Override
    public Single<List<SchoolSatScore>> getSatForSchool(String dbn) {
        return nycSchoolService.getSatForSchool(dbn);
    }
}
