package com.challenge.nycschools.ui.base;

import androidx.annotation.StringRes;

public interface BaseMvpView {

    void showLoading();

    void hideLoading();

    void onError(@StringRes int resId);

    void showMessage(@StringRes int resId);
}
