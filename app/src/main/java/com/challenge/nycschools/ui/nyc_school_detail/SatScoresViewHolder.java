package com.challenge.nycschools.ui.nyc_school_detail;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.challenge.nycschools.R;
import com.challenge.nycschools.data.pojo.SchoolSatScore;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

class SatScoresViewHolder extends RecyclerView.ViewHolder {

    public static final String NOT_APPLICABLE = "N/A";

    @BindView(R.id.school_name_text)
    TextView schoolNameText;
    @BindView(R.id.sat_takers_text)
    TextView satTakersText;
    @BindView(R.id.avg_reading_score_text)
    TextView averageCriticalReadingScoreText;
    @BindView(R.id.avg_math_score_text)
    TextView averageMathScoreText;
    @BindView(R.id.avg_writing_score_text)
    TextView averageWritingScoreText;

    private Context context;

    public SatScoresViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        context = itemView.getContext();
    }

    public void bind(SchoolSatScore satScore) {

        String satTakers = (satScore.getNumOfSatTestTakers() != null && !satScore.getNumOfSatTestTakers().isEmpty())
                ? satScore.getNumOfSatTestTakers() : NOT_APPLICABLE;

        String averageCriticalReadingScore = (satScore.getSatCriticalReadingAvgScore() != null && !satScore.getSatCriticalReadingAvgScore().isEmpty())
                ? satScore.getSatCriticalReadingAvgScore() : NOT_APPLICABLE;

        String averageMathScore = (satScore.getSatMathAvgScore() != null && !satScore.getSatMathAvgScore().isEmpty())
                ? satScore.getSatMathAvgScore() : NOT_APPLICABLE;

        String averageWritingScore = (satScore.getSatWritingAvgScore() != null && !satScore.getSatWritingAvgScore().isEmpty())
                ? satScore.getSatWritingAvgScore() : NOT_APPLICABLE;

        schoolNameText.setText(satScore.getSchoolName());
        satTakersText.setText(context.getString(R.string.number_of_sat_takers, satTakers));
        averageCriticalReadingScoreText.setText(context.getString(R.string.average_critical_reading_score, averageCriticalReadingScore));
        averageMathScoreText.setText(context.getString(R.string.average_math_score, averageMathScore));
        averageWritingScoreText.setText(context.getString(R.string.average_writing_score, averageWritingScore));
    }
}
