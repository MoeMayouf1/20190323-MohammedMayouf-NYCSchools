package com.challenge.nycschools.ui.nyc_school_detail;

import com.challenge.nycschools.ui.base.BaseMvpView;

public interface SchoolDetailsView extends BaseMvpView {
    void showSchoolAddress(String address);

    void showSchoolEmail(String email);

    void showPhoneNumber(String phoneNumber);

    void showSchoolWebsite(String website);
}
