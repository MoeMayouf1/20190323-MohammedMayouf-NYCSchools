package com.challenge.nycschools.ui.nyc_schools;

import com.challenge.nycschools.ui.base.BasePresenterImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NYSchoolsFragmentModule {

    @Provides
    @Singleton
    BasePresenterImpl<NYSchoolsView> provideNYSchoolsPresenter(NYSchoolsPresenter presenter) {
        return presenter;
    }

}
