package com.challenge.nycschools.ui.nyc_schools;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.challenge.nycschools.R;
import com.challenge.nycschools.data.pojo.NYCSchool;
import com.challenge.nycschools.data.pojo.NYSchoolDetail;
import com.challenge.nycschools.ui.base.BaseFragment;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class NYSchoolsFragment extends BaseFragment implements NYSchoolsView, NYSchoolsAdapter.AdapterInteractionListener {

    interface FragmentInteractionListener {
        void showSchoolDetailScreen(NYSchoolDetail schoolDetail);
    }

    private FragmentInteractionListener listener;

    @Inject
    NYSchoolsPresenter presenter;

    @BindView(R.id.progress_circular)
    ProgressBar progressBar;
    @BindView(R.id.nyschools_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.container)
    FrameLayout parent;

    private NYSchoolsAdapter adapter;

    public NYSchoolsFragment() {
    }

    public static NYSchoolsFragment newInstance() {
        return new NYSchoolsFragment();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_nyschools;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        presenter.onAttach(this);
        try {
            listener = (FragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement FragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retain this fragment across configuration changes.
        setRetainInstance(true);
        adapter = new NYSchoolsAdapter(this);
    }

    @Override
    protected void initView(View view) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter.getItemCount() == 0) {
            presenter.getNYSchools();
        }
    }

    @Override
    public void showLoading() {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
    }


    @Override
    public void onError(int resId) {
        if (getActivity() != null) {
            showSnackBar(getActivity().getString(resId));
        }
    }

    @Override
    public void showMessage(int resId) {
        if (getActivity() != null) {
            showSnackBar(getActivity().getString(resId));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.onDetach();
    }

    @Override
    public void setSchoolsList(List<NYCSchool> nycSchools) {
        adapter.setNycSchoolList(nycSchools);
    }

    @Override
    public void onSeeMoreClicked(NYCSchool school) {
        presenter.onSchoolSelected(school);
    }

    @Override
    public void showSchoolDetail(NYSchoolDetail schoolDetail) {
        listener.showSchoolDetailScreen(schoolDetail);
    }

    private void showSnackBar(String message) {
        Snackbar.make(parent, message, Snackbar.LENGTH_LONG).show();
    }
}
