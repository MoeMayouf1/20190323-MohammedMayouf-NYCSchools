package com.challenge.nycschools.ui.nyc_schools;

import com.challenge.nycschools.data.pojo.NYCSchool;
import com.challenge.nycschools.data.pojo.NYSchoolDetail;
import com.challenge.nycschools.ui.base.BaseMvpView;

import java.util.List;

public interface NYSchoolsView extends BaseMvpView {

    void setSchoolsList(List<NYCSchool> nycSchools);

    void showSchoolDetail(NYSchoolDetail schoolDetail);
}
