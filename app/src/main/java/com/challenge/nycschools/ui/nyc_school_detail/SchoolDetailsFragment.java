package com.challenge.nycschools.ui.nyc_school_detail;


import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.challenge.nycschools.R;
import com.challenge.nycschools.data.pojo.NYSchoolDetail;
import com.challenge.nycschools.ui.base.BaseFragment;

import javax.inject.Inject;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class SchoolDetailsFragment extends BaseFragment implements SchoolDetailsView {

    private static final String SCHOOL_DETAILS_PARAM = "SCHOOL_DETAILS_PARAM";

    private NYSchoolDetail schoolDetail;

    @Inject
    SchoolDetailsPresenter presenter;

    @BindView(R.id.address_text)
    TextView addressTextView;
    @BindView(R.id.email_text)
    TextView emailTextView;
    @BindView(R.id.phone_number_text)
    TextView phoneNumberTextView;
    @BindView(R.id.website_text)
    TextView websiteTextView;

    @BindView(R.id.sat_scores_recyclerview)
    RecyclerView satScoresRecyclerView;


    public SchoolDetailsFragment() {
        // Required empty public constructor
    }

    public static SchoolDetailsFragment newInstance(NYSchoolDetail schoolDetail) {
        SchoolDetailsFragment fragment = new SchoolDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(SCHOOL_DETAILS_PARAM, schoolDetail);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.onAttach(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            schoolDetail = getArguments().getParcelable(SCHOOL_DETAILS_PARAM);
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_school_details;
    }

    @Override
    protected void initView(View view) {
        satScoresRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        satScoresRecyclerView.setAdapter(new SatScoresAdapter(schoolDetail.getSatScores()));

        presenter.getSchoolDetails(schoolDetail);
    }

    @Override
    public void showLoading() {
        // DO NOTHING
    }

    @Override
    public void hideLoading() {
        // DO NOTHING
    }

    @Override
    public void onError(int resId) {
        // DO NOTHING
    }

    @Override
    public void showMessage(int resId) {
        // DO NOTHING
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.onDetach();
    }

    @Override
    public void showSchoolAddress(String address) {
        addressTextView.setText(address);
    }

    @Override
    public void showSchoolEmail(String email) {
        emailTextView.setText(email);
    }

    @Override
    public void showPhoneNumber(String phoneNumber) {
        phoneNumberTextView.setText(phoneNumber);
    }

    @Override
    public void showSchoolWebsite(String website) {
        // Ideally, the website would be clickable so that a WebView would be open and the
        // website would be shown when the link is clicked
        websiteTextView.setText(website);
    }
}
