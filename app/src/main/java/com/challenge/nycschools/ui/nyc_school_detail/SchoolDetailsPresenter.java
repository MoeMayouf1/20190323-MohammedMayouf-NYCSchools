package com.challenge.nycschools.ui.nyc_school_detail;

import com.challenge.nycschools.data.pojo.NYCSchool;
import com.challenge.nycschools.data.pojo.NYSchoolDetail;
import com.challenge.nycschools.ui.base.BasePresenterImpl;

import javax.inject.Inject;

public class SchoolDetailsPresenter extends BasePresenterImpl<SchoolDetailsView> {

    @Inject
    public SchoolDetailsPresenter() {
    }

    public void getSchoolDetails(NYSchoolDetail schoolDetail) {
        NYCSchool school = schoolDetail.getSchool();
        showSchoolAddress(school);
        showSchoolEmail(school.getSchoolEmail());
        showSchoolPhoneNumber(school.getPhoneNumber());
        showSchoolWebsite(school.getWebsite());
    }

    /*
     A better approach would be to show and hide these (address, email, phoneNumber, Website)
     based on their values. If they are empty, the view that displays
     them should not be visible.
     */

    private void showSchoolAddress(NYCSchool school) {
        StringBuilder addressBuilder = new StringBuilder();
        addressBuilder.append(school.getPrimaryAddressLine1())
                .append("\n")
                .append(school.getCity())
                .append(" ")
                .append(school.getStateCode())
                .append("\n")
                .append(school.getZip());
        view.showSchoolAddress(addressBuilder.toString());
    }

    private void showSchoolEmail(String email) {
        view.showSchoolEmail(email);
    }

    private void showSchoolPhoneNumber(String phoneNumber) {
        String phoneNumberBuilder = "+1 " +
                phoneNumber;
        view.showPhoneNumber(phoneNumberBuilder);
    }


    private void showSchoolWebsite(String website) {
        view.showSchoolWebsite(website);
    }
}
