package com.challenge.nycschools.ui.nyc_schools;

import com.challenge.nycschools.R;
import com.challenge.nycschools.common.SchedulerProvider;
import com.challenge.nycschools.data.pojo.NYCSchool;
import com.challenge.nycschools.data.pojo.NYSchoolDetail;
import com.challenge.nycschools.data.service.base.DataManager;
import com.challenge.nycschools.ui.base.BasePresenterImpl;

import javax.inject.Inject;

public class NYSchoolsPresenter extends BasePresenterImpl<NYSchoolsView> {

    private final SchedulerProvider schedulerProvider;
    private final DataManager dataManager;

    @Inject
    public NYSchoolsPresenter(SchedulerProvider schedulerProvider, DataManager dataManager) {
        this.schedulerProvider = schedulerProvider;
        this.dataManager = dataManager;
    }

    void getNYSchools() {
        addDisposable(dataManager.getNYSchools()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe(disposable -> view.showLoading())
                .doFinally(() -> view.hideLoading())
                .subscribe(nycSchools -> view.setSchoolsList(nycSchools),
                        throwable -> view.showMessage(R.string.error_message)));
    }

    void onSchoolSelected(NYCSchool school) {
        addDisposable(dataManager.getSatForSchool(school.getDbn())
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .filter(satScores -> !satScores.isEmpty())
                .map(satScores -> new NYSchoolDetail(school, satScores))
                .doOnSubscribe(disposable -> view.showLoading())
                .doFinally(() -> view.hideLoading())
                .subscribe(schoolDetail -> view.showSchoolDetail(schoolDetail), // success
                        throwable -> view.onError(R.string.error_message), // error
                        () -> view.showMessage(R.string.no_sat_scores))); // empty
    }
}
