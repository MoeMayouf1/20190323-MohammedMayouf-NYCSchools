package com.challenge.nycschools.ui.base;

public interface BasePresenter<V extends BaseMvpView> {

    void onAttach(V mvpView);

    void onDetach();

}
