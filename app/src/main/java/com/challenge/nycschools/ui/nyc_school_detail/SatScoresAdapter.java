package com.challenge.nycschools.ui.nyc_school_detail;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.challenge.nycschools.R;
import com.challenge.nycschools.data.pojo.SchoolSatScore;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SatScoresAdapter extends RecyclerView.Adapter<SatScoresViewHolder> {

    private List<SchoolSatScore> satScoreList = new ArrayList<>();

    public SatScoresAdapter(List<SchoolSatScore> satScoreList) {
        this.satScoreList = satScoreList;
    }

    @NonNull
    @Override
    public SatScoresViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.sat_scores_recycler_view_item, parent, false);
        return new SatScoresViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SatScoresViewHolder holder, int position) {
        SchoolSatScore satScore = satScoreList.get(position);
        holder.bind(satScore);
    }

    @Override
    public int getItemCount() {
        return satScoreList.size();
    }

}
