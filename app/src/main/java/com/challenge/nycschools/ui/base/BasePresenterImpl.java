package com.challenge.nycschools.ui.base;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BasePresenterImpl<V extends BaseMvpView> implements BasePresenter<V>{

    protected V view;
    protected CompositeDisposable compositeDisposable;

    @Override
    public void onAttach(V mvpView) {
        compositeDisposable = new CompositeDisposable();
        view = mvpView;
    }

    @Override
    public void onDetach() {
        view = null;
        compositeDisposable.dispose();
    }

    public V getView() {
        return view;
    }

    protected void addDisposable(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

}
