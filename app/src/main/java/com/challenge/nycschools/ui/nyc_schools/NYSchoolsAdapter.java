package com.challenge.nycschools.ui.nyc_schools;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.challenge.nycschools.R;
import com.challenge.nycschools.data.pojo.NYCSchool;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NYSchoolsAdapter extends RecyclerView.Adapter<NYSchoolsAdapterViewHolder> {

    private List<NYCSchool> nycSchoolList = new ArrayList<>();

    interface AdapterInteractionListener {
        void onSeeMoreClicked(NYCSchool school);
    }

    private AdapterInteractionListener listener;

    public NYSchoolsAdapter(AdapterInteractionListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public NYSchoolsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.schools_recycler_view_item, parent, false);
        return new NYSchoolsAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NYSchoolsAdapterViewHolder holder, int position) {
        NYCSchool school = nycSchoolList.get(position);
        holder.bind(school, listener);
    }

    @Override
    public int getItemCount() {
        return nycSchoolList.size();
    }

    public void setNycSchoolList(List<NYCSchool> schoolList) {
        if (!nycSchoolList.isEmpty()) {
            nycSchoolList.clear();
        }
        nycSchoolList.addAll(schoolList);
        notifyDataSetChanged();
    }
}
