package com.challenge.nycschools.ui.nyc_school_detail;

import com.challenge.nycschools.ui.base.BasePresenterImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SchoolDetailsFragmentModule {

    @Provides
    @Singleton
    BasePresenterImpl<SchoolDetailsView> getSchoolDetailPresenter(SchoolDetailsPresenter presenter) {
        return presenter;
    }
}
