package com.challenge.nycschools.ui.nyc_schools;

import android.os.Bundle;

import com.challenge.nycschools.R;
import com.challenge.nycschools.data.pojo.NYSchoolDetail;
import com.challenge.nycschools.ui.base.BaseActivity;
import com.challenge.nycschools.ui.nyc_school_detail.SchoolDetailsFragment;

public class NYSchoolsActivity extends BaseActivity implements NYSchoolsFragment.FragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void addFragment(Bundle savedInstance) {
        if (savedInstance == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.ui_container, NYSchoolsFragment.newInstance())
                    .commit();
        }
    }

    @Override
    public void showSchoolDetailScreen(NYSchoolDetail schoolDetail) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.ui_container, SchoolDetailsFragment.newInstance(schoolDetail))
                .addToBackStack(null)
                .commit();

    }
}
