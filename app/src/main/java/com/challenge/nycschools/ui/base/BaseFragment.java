package com.challenge.nycschools.ui.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

public abstract class BaseFragment extends DaggerFragment {

    @LayoutRes
    protected abstract int getLayout();
    protected abstract void initView(View view);

    private BaseActivity baseActivity;
    private Unbinder butterKnifeUnbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        butterKnifeUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    @Override
    public void onDestroy() {
        baseActivity = null;
        super.onDestroy();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (butterKnifeUnbinder != null) {
            butterKnifeUnbinder.unbind();
            butterKnifeUnbinder = null;
        }
    }
}
