package com.challenge.nycschools.ui.nyc_schools;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.challenge.nycschools.R;
import com.challenge.nycschools.data.pojo.NYCSchool;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

class NYSchoolsAdapterViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.school_info_text)
    TextView schoolInfoTextView;
    @BindView(R.id.school_overview_text)
    TextView schoolOverViewText;
    @BindView(R.id.see_more_button)
    Button seeMoreButton;

    public NYSchoolsAdapterViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(NYCSchool nycSchool, NYSchoolsAdapter.AdapterInteractionListener listener) {
        String schoolName = nycSchool.getSchoolName() != null ? nycSchool.getSchoolName() : "";
        String schoolCity = nycSchool.getCity() != null ? nycSchool.getCity() : "";
        String overview = nycSchool.getOverviewParagraph() != null ? nycSchool.getOverviewParagraph() : "";

        setSchoolInfoTextView(schoolInfoTextView, schoolName, schoolCity);
        setSchoolOverViewTextView(schoolOverViewText, overview);

        seeMoreButton.setOnClickListener(view -> listener.onSeeMoreClicked(nycSchool));
    }

    private void setSchoolInfoTextView(TextView textView, String schoolName, String city) {
        if (!schoolName.isEmpty()) {
            textView.setText(schoolName.concat(", ").concat(city));
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    private void setSchoolOverViewTextView(TextView textView, String overView) {
        if (!overView.isEmpty()) {
            textView.setText(overView);
        } else {
            textView.setVisibility(View.GONE);
        }
    }
}
